-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 29 Paź 2015, 15:56
-- Wersja serwera: 5.6.26
-- Wersja PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `licealiada`
--

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `group`
--
CREATE TABLE IF NOT EXISTS `group` (
`full_name` varchar(100)
,`imie` text
,`nazwisko` text
,`suma_pkt` int(3)
,`pkt` int(4)
,`plec` tinyint(1)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `open`
--

CREATE TABLE IF NOT EXISTS `open` (
  `nr` int(3) NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `szkola` varchar(9) COLLATE utf8_polish_ci NOT NULL,
  `t1` int(3) NOT NULL,
  `t2` int(3) NOT NULL,
  `t3` int(3) NOT NULL,
  `t4` int(3) NOT NULL,
  `t5` int(3) NOT NULL,
  `t6` int(3) NOT NULL,
  `t7` int(3) NOT NULL,
  `t8` int(3) NOT NULL,
  `t9` int(3) NOT NULL,
  `t10` int(3) NOT NULL,
  `suma_pkt` int(3) NOT NULL,
  `x10` int(2) NOT NULL,
  `plec` tinyint(1) NOT NULL,
  `tip` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `open`
--

INSERT INTO `open` (`nr`, `imie`, `nazwisko`, `szkola`, `t1`, `t2`, `t3`, `t4`, `t5`, `t6`, `t7`, `t8`, `t9`, `t10`, `suma_pkt`, `x10`, `plec`, `tip`) VALUES
(1, 'Monika', 'Grodzka', 'VLO', 10, 1, 12, 13, 24, 1, 20, 13, 18, 1, 113, 1, 0, 1),
(2, 'Marta', 'Dabrowska', 'VLO', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 0, 0, 1),
(3, 'Marta', 'Dabrowska', 'VLO', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `szkola`
--

CREATE TABLE IF NOT EXISTS `szkola` (
  `skrot` varchar(9) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `pkt` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `szkola`
--

INSERT INTO `szkola` (`skrot`, `full_name`, `pkt`) VALUES
('Grodno', 'szkola w grodnie', 0),
('VILO', 'VI Liceum', 0),
('VLO', 'V Liceum', 133);

-- --------------------------------------------------------

--
-- Struktura widoku `group`
--
DROP TABLE IF EXISTS `group`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `group` AS select `s`.`full_name` AS `full_name`,`o`.`imie` AS `imie`,`o`.`nazwisko` AS `nazwisko`,`o`.`suma_pkt` AS `suma_pkt`,`s`.`pkt` AS `pkt`,`o`.`plec` AS `plec` from (`open` `o` join `szkola` `s` on((`o`.`szkola` = `s`.`skrot`))) where (`o`.`tip` = 1) order by `s`.`pkt` desc,`o`.`suma_pkt` desc,`s`.`full_name` desc;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `open`
--
ALTER TABLE `open`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `szkola` (`szkola`);

--
-- Indexes for table `szkola`
--
ALTER TABLE `szkola`
  ADD PRIMARY KEY (`skrot`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `open`
--
ALTER TABLE `open`
  ADD CONSTRAINT `open_ibfk_1` FOREIGN KEY (`szkola`) REFERENCES `szkola` (`skrot`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
